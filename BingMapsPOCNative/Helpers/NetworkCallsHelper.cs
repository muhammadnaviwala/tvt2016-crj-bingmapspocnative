﻿using System;
using System.Threading;
using System.Net;
using System.IO;

namespace BingMapsPOCNative.Helpers
{
    public static class NetworkCallsHelper
    {
        private static AutoResetEvent _stopped = new AutoResetEvent(false);

        public static string GetResponseFromUri(string uri)
        {
            string responseToReturn = "";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = "GET";

            GetResponseAsync(request, (response) =>
                {
                    responseToReturn = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    _stopped.Set();
                });
            _stopped.WaitOne();

            return responseToReturn;
        }

        private static void GetResponseAsync(HttpWebRequest webRequest, Action<HttpWebResponse> responseAction)
        {
            Action wrapperAction = () =>
                {
                    webRequest.BeginGetResponse(new AsyncCallback((result) =>
                        {
                            var response = (HttpWebResponse)((HttpWebRequest)result.AsyncState).EndGetResponse(result);
                            responseAction(response);
                        }), webRequest);
                };
            wrapperAction.BeginInvoke(new AsyncCallback((result) =>
                {
                    var action = (Action)result.AsyncState;
                    action.EndInvoke(result);
                }), wrapperAction);
        }
    }
}

