﻿using System;
using System.Collections.Generic;

namespace BingMapsPOCNative.Helpers
{
    public static class UrlHelper
    {
        public static string CreateStringFromParameters(IDictionary<string, string> parametersDictionary)
        {
            if (parametersDictionary == null || parametersDictionary.Count == 0)
            {
                return "";
            }

            var list = new List<string>();
            foreach(var item in parametersDictionary)
            {
                list.Add(item.Key + "=" + item.Value);
            }
            return string.Join("&", list);
        }
    }
}

