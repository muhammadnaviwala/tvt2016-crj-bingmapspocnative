﻿using System;
using System.Collections.Generic;
using BingMapsPOCNative.Models;

namespace BingMapsPOCNative.Interfaces
{
    public interface IRealTimeRouteProvider
    {
        void SetStartAndEndPositions(Position start, Position end);
        void SetUrl(string baseUrl, Dictionary<string, string> urlParameters);
        void GetResponseFromUrl (string url);
        List<Position> GetRoutePoints();
        string GetEta();
        void LoadData ();
    }
}

