﻿using System;

namespace BingMapsPOCNative.Models
{
    public class Position
    {
        public double Latitude { get; private set; }

        public double Longitude { get; private set; }

        public Position(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }
    }
}

