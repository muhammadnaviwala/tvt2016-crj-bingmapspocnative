﻿using System;
using System.Collections.Generic;
using BingMapsPOCNative.Interfaces;
using BingMapsPOCNative.Models;
using BingMapsPOCNative.Helpers;
using Newtonsoft.Json;

namespace BingMapsPOCNative.InterfaceImplementation
{
    public class RealTimeRouteProviderBing : IRealTimeRouteProvider
    {
        private Dictionary<string, string> urlParameters = new Dictionary<string, string> ();
        private string baseUrl = string.Empty;
        private string url = string.Empty;
        private Position startPosition;
        private Position endPosition;
        private dynamic jsonResponse;

        public RealTimeRouteProviderBing ()
        {
        }

        #region IRealTimeRouteProvider implementation

        public void SetStartAndEndPositions (Position start, Position end)
        {
            startPosition = start;
            endPosition = end;
        }

        public void SetUrl (string baseUrl, Dictionary<string, string> urlParameters)
        {
            url = baseUrl + UrlHelper.CreateStringFromParameters (urlParameters);
        }

        public void GetResponseFromUrl (string url)
        {
            string response = NetworkCallsHelper.GetResponseFromUri (url);
            jsonResponse = JsonConvert.DeserializeObject(response);
        }

        public List<Position> GetRoutePoints ()
        {
            List<Position> listToReturn = new List<Position> ();
            if(jsonResponse != null)
            {
                var resourceSets = jsonResponse ["resourceSets"];
                var firstResourceSet = resourceSets [0];
                var resources = firstResourceSet ["resources"];
                var firstResource = resources [0];
                var routePath = firstResource ["routePath"];
                var line = routePath ["line"];
                var coordinates = line ["coordinates"];
                foreach (var coordinate in coordinates)
                {
                    listToReturn.Add (new Position (Convert.ToDouble(coordinate [0].ToString()), Convert.ToDouble(coordinate [1].ToString())));

                }
            }
            return listToReturn;
        }

        public string GetEta ()
        {
            if(jsonResponse != null)
            {
                var resourceSets = jsonResponse ["resourceSets"];
                var firstResourceSet = resourceSets [0];
                var resources = firstResourceSet ["resources"];
                var firstResource = resources [0];
                var travelDurationTraffic = firstResource ["travelDurationTraffic"];

                return travelDurationTraffic.ToString();
            }
            return "";
        }

        public void LoadData()
        {
            baseUrl = "http://dev.virtualearth.net/REST/V1/Routes/Driving?";

            urlParameters = new Dictionary<string, string> ();
            urlParameters.Add ("wp.0", startPosition.Latitude.ToString() + "," + startPosition.Longitude.ToString());
            urlParameters.Add ("wp.1", endPosition.Latitude.ToString() + "," + endPosition.Longitude.ToString());
            urlParameters.Add ("avoid", "minimizeTolls");
            urlParameters.Add ("optimize", "timeWithTraffic");
            //urlParameters.Add ("ra", "excludeItinerary");
            urlParameters.Add ("ra", "routePath");
            urlParameters.Add ("key", "Ar-KwlXtY9ibON7VdD-PNse2qa3gehp9htzN_8QBym3kM3kEC1vBuzyUyKTn2QuE");

            SetUrl (baseUrl, urlParameters);
            GetResponseFromUrl (url);
        }

        #endregion
    }
}

