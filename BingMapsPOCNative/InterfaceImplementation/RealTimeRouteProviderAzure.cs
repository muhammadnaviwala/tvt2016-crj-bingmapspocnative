﻿using System;
using BingMapsPOCNative.Interfaces;
using System.Collections.Generic;
using BingMapsPOCNative.Models;
using BingMapsPOCNative.Helpers;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace BingMapsPOCNative.InterfaceImplementation
{
    public class RealTimeRouteProviderAzure : IRealTimeRouteProvider
    {
        string baseUrl = string.Empty;
        string url = string.Empty;
        dynamic jsonResponse = string.Empty;

        public RealTimeRouteProviderAzure()
        {
        }

        #region IRealTimeRouteProvider implementation

        public void SetStartAndEndPositions(Position start, Position end)
        {
            // do nothing in this implementation
        }

        public void SetUrl(string baseUrl, Dictionary<string, string> urlParameters)
        {
            url = baseUrl + UrlHelper.CreateStringFromParameters (urlParameters);
        }

        public void GetResponseFromUrl(string url)
        {
            string response = NetworkCallsHelper.GetResponseFromUri (url);
            jsonResponse = JsonConvert.DeserializeObject(response);
        }

        public List<Position> GetRoutePoints()
        {
            List<Position> listToReturn = new List<Position> ();
            if(jsonResponse != null)
            {
                var waypoints = jsonResponse["Waypoints"];
                foreach (var waypoint in waypoints)
                {
                    listToReturn.Add (new Position (Convert.ToDouble(waypoint["Latitude"]), Convert.ToDouble(waypoint["Longitude"])));
                }
            }

            return listToReturn;
        }

        public string GetEta()
        {
            if(jsonResponse != null)
            {
                return jsonResponse ["Duration"].ToString();
            }
            return "";
        }

        public void LoadData()
        {
            baseUrl = "http://crj-poc-1.azurewebsites.net/api/routes/1";
            SetUrl (baseUrl, null);
            GetResponseFromUrl (url);
        }

        #endregion
    }
}

