// WARNING
//
// This file has been generated automatically by Xamarin Studio Enterprise to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace BingMapsPOCNative.iOS
{
	[Register ("NativeMapController")]
	partial class NativeMapController
	{
		[Outlet]
		UIKit.UITextField EndPositionTextBox { get; set; }

		[Outlet]
		UIKit.UIButton EnterDefaultDataButton { get; set; }

		[Outlet]
		UIKit.UILabel EtaText { get; set; }

		[Outlet]
		MapKit.MKMapView MapView { get; set; }

		[Outlet]
		UIKit.UIButton ReloadMapButton { get; set; }

		[Outlet]
		UIKit.UITextField StartPositionTextBox { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (EndPositionTextBox != null) {
				EndPositionTextBox.Dispose ();
				EndPositionTextBox = null;
			}

			if (MapView != null) {
				MapView.Dispose ();
				MapView = null;
			}

			if (ReloadMapButton != null) {
				ReloadMapButton.Dispose ();
				ReloadMapButton = null;
			}

			if (StartPositionTextBox != null) {
				StartPositionTextBox.Dispose ();
				StartPositionTextBox = null;
			}

			if (EnterDefaultDataButton != null) {
				EnterDefaultDataButton.Dispose ();
				EnterDefaultDataButton = null;
			}

			if (EtaText != null) {
				EtaText.Dispose ();
				EtaText = null;
			}
		}
	}
}
