﻿using System;
using UIKit;
using BingMapsPOCNative.Interfaces;
using BingMapsPOCNative.Models;
using System.Collections.Generic;
using BingMapsPOCNative.InterfaceImplementation;
using BingMapsPOCNative.iOS.Controls;
using CoreLocation;
using MapKit;
using System.Text.RegularExpressions;
using System.Linq;

namespace BingMapsPOCNative.iOS
{
    public partial class NativeMapController : UIViewController
    {
        //IRealTimeRouteProvider realTimeRouteProvider = new RealTimeRouteProviderBing();
        IRealTimeRouteProvider realTimeRouteProvider = new RealTimeRouteProviderAzure();
        MKPolylineRenderer polylineRenderer;

        public NativeMapController(IntPtr handle)
            : base(handle)
        {       
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            ReloadMapButton.TouchUpInside += ReloadButtonAction;
            EnterDefaultDataButton.TouchUpInside += EnterDefaultDataButtonAction;
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        public void LoadMapData(Position startPosition, Position endPosition)
        {
            realTimeRouteProvider.SetStartAndEndPositions(startPosition, endPosition);
            realTimeRouteProvider.LoadData();

            List<Position> routePoints = realTimeRouteProvider.GetRoutePoints();
            if (MapView.Annotations != null && MapView.Annotations.Length > 0)
            {
                MapView.RemoveAnnotations(MapView.Annotations);
            }
            MapView.AddAnnotation(new BasicMapAnnotation(new CLLocationCoordinate2D(startPosition.Latitude, startPosition.Longitude), "Start", startPosition.Latitude + ", " + startPosition.Longitude));
            MapView.AddAnnotation(new BasicMapAnnotation(new CLLocationCoordinate2D(endPosition.Latitude, endPosition.Longitude), "End", endPosition.Latitude + ", " + endPosition.Longitude));

            CLLocationCoordinate2D[] coords = new CLLocationCoordinate2D[routePoints.Count];

            int index = 0;
            foreach (var position in routePoints) {
                coords [index] = new CLLocationCoordinate2D (position.Latitude, position.Longitude);
                index++;
            }

            MapView.OverlayRenderer = GetOverlayRenderer;
            var routeOverlay = MKPolyline.FromCoordinates (coords);
            if (MapView.Overlays != null && MapView.Overlays.Length > 0)
            {
                MapView.RemoveOverlays(MapView.Overlays);
            }
            MapView.AddOverlay (routeOverlay);
            
            CenterMapRegion(startPosition);

            double etaInSeconds = Convert.ToDouble(realTimeRouteProvider.GetEta ());
            EtaText.Text = string.Format ("{0:0} minutes and {1:00} seconds", (etaInSeconds % 3600) / 60, (etaInSeconds % 3600) % 60);
        }

        public void CenterMapRegion(Position position)
        {
            MKCoordinateRegion region = new MKCoordinateRegion(new CLLocationCoordinate2D(position.Latitude, position.Longitude), new MKCoordinateSpan(0.05, 0.05));
            MapView.SetRegion(region, true);
        }

        MKOverlayRenderer GetOverlayRenderer (MKMapView mapView, IMKOverlay overlay)
        {
            polylineRenderer = new MKPolylineRenderer (overlay as MKPolyline);
            polylineRenderer.StrokeColor = UIColor.Red;
            polylineRenderer.LineWidth = 5;
            return polylineRenderer;
        }

        void ReloadButtonAction(object sender, EventArgs e)
        {
            if (CoordinatesAreValid(StartPositionTextBox.Text) && CoordinatesAreValid(EndPositionTextBox.Text))
            {
                LoadMapData(StringToPosition(StartPositionTextBox.Text), StringToPosition(EndPositionTextBox.Text));
            }
            else
            {
                ShowAlertViewWithOk("Error", "Please enter valid Coordinates for Start Position and End Position");
            }
            this.View.EndEditing(true);
        }

        bool CoordinatesAreValid(string coordinates)
        {
            Regex regex = new Regex(@"^(\-?\d+(\.\d+)?),\w*(\-?\d+(\.\d+)?)$");
            Match match = regex.Match(coordinates);
            return match.Success;
        }

        Position StringToPosition(string coordinates)
        {
            string[] coordinatesValues = coordinates.Split(',').Select(sValue => sValue.Trim()).ToArray();
            return new Position(Convert.ToDouble(coordinatesValues[0]), Convert.ToDouble(coordinatesValues[1]));
        }

        void ShowAlertViewWithOk(string title, string message)
        {
            UIAlertView alert = new UIAlertView () { 
                Title = title, Message = message
            };
            alert.AddButton("OK");
            alert.Show ();
        }

        void EnterDefaultDataButtonAction(object sender, EventArgs e)
        {
            StartPositionTextBox.Text = "29.730154,-95.440920"; // Pariveda office
            EndPositionTextBox.Text = "29.740602,-95.464309"; // Galleria
        }
    }
}


