﻿using System;

using UIKit;

namespace BingMapsPOCNative.iOS
{
    public partial class XamarinMapController : UIViewController
    {
        public XamarinMapController(IntPtr handle)
            : base(handle)
        {       
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}


